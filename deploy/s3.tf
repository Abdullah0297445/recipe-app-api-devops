resource "aws_s3_bucket" "app_public_files" {
  bucket        = "${local.prefix}-my-very-unique-s3-bucket"
  acl           = "public-read"
  force_destroy = true
}